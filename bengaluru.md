---
layout: page
title: "Bengaluru - 18th to 20th Jan"
permalink: /bengaluru/
---

### 18th Jan

  * **11 AM to 1 PM**: Public talk at R V College of Engineering (RVCE)
  * **3 PM to 5 PM**: Community interaction and discussion at RVCE

### 19th Jan

  * **11 AM to 1PM**: Talk at IISc (Dept of CSA)

### 20th Jan

#### Venue

FSMK Boot Camp at PES College, Mandya (near Mysore)

More details are available on the [FSMK
Website](https://fsmk.org/events/fsmk_camp_rms_announcement/)

#### Timings

2:00 PM to 5:00 PM

