---
# You don't need to edit this file, it's empty on purpose.
# Edit whiteglass' home layout instead if you want to make some changes.
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: home
title: "5th Jan to 20th Jan, 2019"
---


<img src="{{site.baseurl}}/images/talking.jpg" width="800">

### Tour Span

5th January, 2019 to 20th January, 2019

### Schedule

  * **5th Jan to 9th Jan**, [Delhi](/delhi/)
  * **10th Jan to 13th Jan**, [Kozhikode (Calicut)](/kozhikode/)
  * **14th Jan**, [Kochi (Cochin)](/kochi/)
  * **15th Jan to 17th Jan**, [Trivandrum](/trivandrum/)
  * **18th Jan to 20th Jan**, [Bengaluru](/bengaluru/)

<hr>

### Recent Updates

 <ul class="post-archives">
    {% for post in site.posts %}
      {% capture post_lang %}{{ post.lang | default: site_lang }}{% endcapture %}
      {% capture lang %}{% if post_lang != site_lang %}{{ post_lang }}{% endif %}{% endcapture %}

      <li>
        <h2>
					<span class="post-meta">
          	{{ post.date | date: "%b %-d, %Y" }}
        	</span>
          <a class="post-link" href="{{ post.url | relative_url }}"{% if lang != empty %} lang="{{ lang }}"{% endif %}>{{ post.title | escape }}</a>
        </h2>
      </li>
    {% endfor %}
</ul>
