---
layout: page
title: "Contact"
permalink: /contact/
---

Please feel free to reach out to the following people for help or more
information:

### Bengaluru

  * Renuka Prasad B - <renukaprasadb@rvce.edu.in>
  * Abhas Abhinav - <abhas@deeproot.in>

### Trivandrum

  * Arun M - <arun@space-kerala.org>

### Delhi

  * Abhas Abhinav - <abhas@deeproot.in>
  * Nishant Sharma - <nishant@unmukti.in>

### Volunteering and Website feedback

  * Abhas Abhinav - <abhas@deeproot.in>

