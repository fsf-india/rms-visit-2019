---
layout: page
title: "Registration is now closed"
permalink: /registration/
---

Registration is now closed for the upcoming talk by Dr Stallman at IISc
owing to space contraints at the venue.
