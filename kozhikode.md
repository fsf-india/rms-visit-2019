---
layout: page
title: "Kozhikode - 10th to 13th Jan"
permalink: /kozhikode/
---

In Kozhikode, RMS will be speaking at the [Kerala Literature
Festival](http://keralaliteraturefestival.com/speakers_more.aspx?id=MjM4).
The exact schedule for his talk will be published here once it is
confirmed by the organisers.

### 10th Jan

    <!-- Add details here -->

### 11th Jan

Richard Stallman will be speaking at the Kerala Literature Festival
(2019-01–13). His speech will be nontechnical, admission is gratis,
and the public is encouraged to attend.

#### Talk Topic

**Free software: Freedom in your computing**

> The Free Software Movement campaigns for computer users' freedom to
> cooperate and control their own computing. The Free Software Movement
> developed the GNU operating system, typically used together with the
> kernel Linux, specifically to make these freedoms possible.

#### Location

Kerala Literature festival, Kozhikode Beach, Port Grounds, Kozhikode

### 12th Jan

#### Talk Topic

**Should we have more surveillance than the USSR?**

> Digital technology has enabled governments to impose surveillance that
> Stalin could only dream of, making it next to impossible to talk with
> a reporter undetected. This puts democracy in danger. Stallman will
> present the absolute limit on general surveillance in a democracy, and
> suggest ways to design systems not to collect dossiers on all
> citizens.

#### Location

Kerala Literature festival, Kozhikode Beach, Port Grounds, Kozhikode

### 13th Jan

    <!-- Add details here -->
