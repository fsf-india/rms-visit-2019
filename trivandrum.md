---
layout: page
title: "Trivandrum - 15th Jan to 17th Jan"
permalink: /trivandrum/
---

### 15th Jan

#### Venue

Mascot Hotel, Thiruvananthapuram

#### Time

3:00 PM to 5:00 PM

### 16th Jan

#### Venue

Travancore Hall, Technopark Campus, Thiruvananthapuram

#### Time

3:00 PM to 5:00 PM
