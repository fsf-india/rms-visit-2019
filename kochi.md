---
layout: page
title: "Kochi - 14th Jan"
permalink: /kochi/
---

### 14th Jan

**Computing, Freedom and Privacy**

#### Talk Topic

> The way digital technology is developing, it threatens our freedom,
> within our computers and in the internet. What are the threats? What
> must we change?

#### Location

Biennale Pavilion, Cabral Yard, Fort Kochi, Kochi

