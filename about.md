---
layout: page
title: "About"
permalink: /about/
---

The [Free Software Foundation of India](http://gnu.org.in) (FSF-India) is pleased
to announce Dr Richard Stallman's 2019 tour of India.

## About FSF-India

<img src="{{site.baseurl}}/images/fsf_logo-new.png" align="right"
style="padding-left: 20px">

The Free Software Foundation India (FSF India) is a non-profit
organisation committed to advocating, promoting and propagating the use
and development of free (swatantra) software in India.

Our goal is to ensure the long term adoption of Free Software, and aim
for the day when all software will be free. This includes educating
people about software freedom and convincing them that it is the freedom
that matters. We regard non-free software as a problem to be solved, not
as a solution to any problem. Document Actions

## About Richard M Stallman (RMS)

<img src="{{site.baseurl}}/images/rms-web.jpg" align="left"
style="padding-right: 20px">

Richard Stallman founded the free software movement in 1983 when [he
announced he would develop the GNU operating
system](https://www.gnu.org/gnu/initial-announcement.html), a Unix-like
operating system meant to consist entirely of [free
software](https://gnu.org/philosophy/free-sw.html). He has been the GNU
project's leader ever since. In October 1985 he started the Free
Software Foundation.

Since the mid-1990s, Stallman has spent most of his time in political
advocacy for free software, and spreading the ethical ideas of the
movement, as well as campaigning against both software patents and
dangerous extension of copyright laws. Before that, Richard developed a
number of widely used programs that are components of GNU, including the
original Emacs, the GNU Compiler Collection, the GNU symbolic debugger
(gdb), GNU Emacs, and various others.

Stallman pioneered the concept of copyleft, and is the main author of
the GNU General Public License, the most widely used free software
license.

Stallman graduated from Harvard in 1974 with a BA in physics. During his
college years and after, he worked as a staff hacker at the MIT
Artificial Intelligence Lab, learning operating system development by
doing it. He wrote the first extensible Emacs text editor there in 1975.
He also developed the AI technique of dependency-directed backtracking,
also known as truth maintenance. In January 1984 he resigned from MIT to
start the GNU project.

## Further reading:

  * The GNU Project: <https://www.gnu.org>
  * FSF: <https://www.fsf.org>
  * Defective by Design: <https://www.defectivebydesign.org>
  * Copyleft: <https://copyleft.org>

